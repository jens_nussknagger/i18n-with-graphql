// Klammer muss um den import wenn "named export", keine Klammer bei "default export"

import graphQlQuery from '$lib/graphql/query/posts.graphql?raw';
import { checkResponse, getDataFromGraphQl } from '$lib/utilities/graphql';
import { error } from '@sveltejs/kit';
import { getLocaleFromUrl } from '../../lib/utilities/localeFromUrl';
import type { PageServerLoad } from './$types';

// * von graphql.ts -> möglichkeit einer funktion einen type mitzuegeben indem man sie einer varibale zuweist

// mit cmd + i -> autosuggestions bei z.B. eingabeparametern von funktionen und viele mehr
// stefans weg:

// Schreibweise um eine Arrow-Funktion zu exportieren
//  export const load: PageServerLoad = async ({ url}) => {

//  }

// Wort "export" macht es möglich, dass eine andere Datei diesen Code importieren kann
export const load: PageServerLoad = async function load({ url }) {
	try {
		// die funktion getDataFromGraphQl gibt eine promise zurück
		const responsePromise = getDataFromGraphQl(graphQlQuery, {
			language: getLocaleFromUrl(url),
			postsPerPage: 20
		});
		// wollen mit dem code erst dannw eitermachen, wenn response da ist
		const response = await responsePromise;
		// hier gibt es einen delay
		checkResponse(response);

		// wir warten auf die promise (promise kann resolven oder rejecten) dann destruktern wir das,
		//was wir zurück bekommen unter der annahme, dass hier ein data key enthalten ist
		// data hat den type "PostQuery"
		// "holt sich daten raus" -> liest den body der http response aus
		const { data }: { data: PostsQuery } = await response.json();
		if (!data) {
			throw error(502, 'Unexpected JSON repsonse');
		}
		console.log(`JA HALLO HIER SOLL WQS KOOOOEMN`, data);

		// bei destrukturing kann man variablen umbennen, wenn man einen : verwendet. neu ist was nach dem : steht
		// 47 - return : datenumformung
		const { pages } = data;

		// neue variable posts, hat nichts mit post von oben zu tun
		// arrays sind objekte
		const posts = pages.edges.map((edge) => {
			const { node } = edge;
			const { startEinstellungen } = node;
			const { titelSektion1 } = startEinstellungen;
			return {
				// könnte man auch so schreiben: date: edge.node.date
				// date,
				// slug,
				titelSektion1
				// trim( entfernt leerzeichen und tabs)
				// excerpt: excerpt.trim(),
				// content
			};
		});

		// const { description, title } = generalSettings;

		// könnte man auch so schreiben: const description = generalSettings.description

		// könnte man auch so schreiben:  		return { description: generalSettings.description, title, posts };
		// es wird ein objekt zurückgegeben / es gibt nur 1 return value z.B. 1 struing, ein objekt oder 1 array
		return { posts };
	} catch (err: unknown) {
		const httpError = err as { status: number; message: string };
		if (httpError.message) {
			throw error(httpError.status ?? 500, httpError.message);
		}
		throw error(500, err as string);
	}
};
