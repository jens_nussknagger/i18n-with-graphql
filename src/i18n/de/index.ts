import type { Translation } from '../i18n-types';

const de: Translation = {
	title: 'typesafe-i18n - Svelte Herbst Summit {year}',
	welcome: 'Willkommen zur Website des Luchs',
	spectators: '{0} Zuschauer live',
	summit: {
		schedule: '{0|simpleDate}'
	},
	log: `Dieses Logging wurde von '{fileName}' aufgerufen`
};

export default de;
