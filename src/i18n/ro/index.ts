import type { Translation } from '../i18n-types';

const ro: Translation = {
	title: 'typesafe-i18n - Svelte Summit Autunno {year}',
	welcome: 'Bine ați venit pe site-ul web al linxului',
	spectators: '{0} {{spettatore|spettatori}} in diretta',
	summit: {
		schedule: '{0|simpleDate}'
	},
	log: `Questa protocollazione è stata chiamata da '{fileName}'`
};

export default ro;
