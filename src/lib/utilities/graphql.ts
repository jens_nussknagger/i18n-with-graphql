import { GRAPHQL_ENDPOINT } from '$env/static/private';
import type { PostsQueryVariables } from '$lib/generated/graphql';

import { error } from '@sveltejs/kit';

// Hier wird eine Funktion mit dem Parameter response exportiert. Der Parameter response hat den Type Response)
// : void -> keine Rückgabe
// never -> sagt man, wenn es die Möglichkeit gibt, dass die Funktion einen Error werfen kann
export function checkResponse(response: Response): void | never {
	const { headers, ok } = response; // Destructuring
	if (!ok) {
		throw error(502, 'Bad Gateway'); // wirft Error, wenn er nicht gecatchet wird, dann wird er in der Console angezeigt, wenn er gecatcht wird, wird er von einer anderen Funtkion verarbeitet
	}

	// auf dem Header object gibt eine Funktion die heißt "get". Die nimmt einen Parameter entgegen,
	//in dem Fall den Namen des https Headers. Mich interessiert den http header namens "Content type".
	//es wird überprüft, ob der http content type === "applictaion.json"
	if (!headers.get('content-type')?.includes('application/json')) {
		throw error(502, 'Bad Gateway: expected JSON data from GraphQL backend');
	}
}

// erstellen eines neuen type ALIAS -> quasi eine umbenennung des types
type QueryVariables = PostsQueryVariables;

// asynchrone funktion, die graphqlQuery mit zwei eingabeparameter, einmal query und einmal variables. query ist vom type string, und variables ist vom type QueryVariables.
// jede funktion gibt etwas zurück
// eigentlich sollte es immer einen return type geben, wenn man ein return hat
// bei http gibt es immer einen request und einen response, kein response nur, wenn es netzwerkprobleme gibt oder der server tot ist
// rückgabewert ist eine promise auf eine response -> das bedeutet, dass man erstmal mit einer promise weitermacht, die dann irgendwann zu einem response wird
// spitze klammern -> generic type

// jede funktion hat in TS einen type, der aus den types der eingabeparameter und dem type des rückgabewerts besteht
// types immer mit Großbuchstaben starten

type GraphQlQueryFunction = (query: string, variables?: PostsQueryVariables) => Promise<Response>;

// Einer Funktion kann man keinen type zuweißen, man muss den Umweg über eine Variable gehen siehe +page.server.ts *, also vorab die Funktion in eine Variable packen
export async function getDataFromGraphQl(
	query: string,
	// variables ist ein optionaler Eingabeparameter der Funktion.
	// Falls dieser nicht angegeben wird, wird mit dem "=" ein Default-Parameter, in diesem Fall ein leeres Objekt mitgegeben
	variables: QueryVariables = {}
): Promise<Response> {
	// Das folgende Code-Snippet dient der besseren Lesbarkeit
	// Alternativ könnte das Objekt, das der Variable "requestInit" zugewiesen wird auch direkt als Eingabeparameter der Funktion "fetch" sein

	// "requestInit" ist ein Objekt, mit "method" als key und "POST" als value
	// Das Objekt dient quasi als Anleitung, wie der Request auf einer bestimmten URL vonstatten gehen soll
	const requestInit: RequestInit = {
		// in graph ql gibt es keine GET request -> in REST wäre es ein GET
		method: 'POST',
		// art des bodys, content type z.B. video oder text oder eben json // damit die gegenseite (in diesem fall der server) weiß, was damit zu machen ist
		// der content type header gibt an, was für ein format mitgeschickt wird - fetch kann sowohl vom browser als auch vom server ausgeführt werden
		headers: {
			'content-type': 'application/json'
		},
		// es wird eine methode stringyfy vom objekt JSON aufgerufen.
		// diese methode hat ein eingabeparameter, was ein objekt ist
		// .stringyfy macht aus einem objekt einen string
		// wenn ich im header einen content type "application/json" habe, dann muss auch im body JSON.stringify stehen
		body: JSON.stringify({
			// kann auch so geschrieben werden
			// query: query,
			// variables: variables
			query,
			variables
		})
	};

	// Das Ergebnis der "fetch" Funktion ist der Rückgabewert der Funktion "graphqlQuery"
	// Die Funktion "fetch" hat zwei Eingabeparameter 1) die URL
	// Der zweite Eingabeparameter der "fetch" Funktion definiert, was genau beim REQUEST gemacht werden soll, siehe Objekt oben
	const responsePromise: Promise<Response> = fetch(GRAPHQL_ENDPOINT, requestInit);

	return responsePromise;
}

// Im "response header" bzw "request header", also in den beiden https header, kann es keys geben, die mit einem x anfangen, diese sind custom

// Die Funktion "graphQlQuery" ist eine Hilfsfunktion (deswegen im Ordner utilities), und dient dazu, Daten von graphQL abzuholen
