const validLocales = ['DE', 'EN', 'RO'] as const;
type WwfLocale = (typeof validLocales)[number];

const defaultLocale: WwfLocale = 'EN';

/**
 * Used to extract the locale from a url string
 */
const localeRegex = /\/([a-z]+)/i;

/**
 * Try to extract a valid locale from the given url.
 *
 * If none can be extracted, return the default locale.
 */
export const getLocaleFromUrl = (url: URL): WwfLocale => {
	const localeMatch = localeRegex.exec(url.pathname)?.[1];
	const localeFromUrl = (localeMatch ?? 'en').toUpperCase() as WwfLocale;

	const isValidLocale = validLocales.includes(localeFromUrl);

	const result = isValidLocale ? localeFromUrl : defaultLocale;

	return result;
};
